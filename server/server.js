const path = require('path')

const app = require(path.join(__dirname,'app'))


app.enable('trust proxy')

const port = process.env.PORT || 3000;
const server = app.listen(port, () => {
  console.log(`App running on port ${port}...`);
});

module.exports = server 