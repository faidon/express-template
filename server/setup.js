/*
 * THE app (express instance) flows in this order:
 * 1. setup.js | here app is initialised and environment variables are imported from "config.env"
 * 2. security.js | security measures are set in place (hpp, helmet, rate limiter etc...)
 * 3. testing.js | to perform development tests, logs (e.g. morgan middleware is inserted there)
 * 4. app.js | app logic and routes are developped there
 * 5. server.js | entry point; Server is initialised here
 */

const express = require('express')
const dotenv = require('dotenv')

// Configure environment variables
dotenv.config({
  path: path.join(__dirname, '..', 'config.env'),
})

// Initialise app
const app = express()

module.exports = app
