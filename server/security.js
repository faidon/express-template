const path = require('path')

const rateLimit = require('express-rate-limit');
const helmet = require('helmet');
const mongoSanitize = require('express-mongo-sanitize');
const xss = require('xss-clean');
const hpp = require('hpp');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const compression = require('compression');
const cors = require('cors');

const app = require(path.join(__dirname, 'setup'))

// Enable CORS
app.use( cors() )

// Set security HTTP headers
app.use(helmet())

// Limit requests for the API
const limiter = rateLimit({
  max: 100,
  windowMs: 500,
  messsage: 'Too many requests.'
})
app.user('/api', limiter)

// Body, cookie parser
app.use(express.json({ limit: '10kb' }))
app.use(express.urlencoded({ extended: true, limit: '10kb' }))
const cookiesSecret = process.env.COOKIES_SECRET
app.use(cookieParser(cookiesSecret))

// Sanitize against no-Sql injections: 
app.use(mongoSanitize())

// Data sanitization against XSS
app.use(xss())

// Prevent parameter (url queries) pollution: 
app.use(hpp({
  whitelist: []
}))

app.use(compression())

module.exports = app