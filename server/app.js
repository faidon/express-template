const path = require('path')

const app = require(path.join(__dirname, 'testing'))
const operational = require(path.join(
  __dirname,
  '..',
  'utils',
  'operationalError',
))
const apiRouter = require(path.join(__dirname, '..', 'api', 'apiRouter'))
const errorRouter = require(path.join(__dirname, '..', 'routes', 'errorRouter'))

// serve static files
app.user(express.static(path.join(__dirname, '..', 'public', 'static')))

// routes:
app.use('/api', apiRouter)

app.use('/dummy', require('../routes/dummyRouter'))

app.all('*', (req, res, next) => {
  next(new operational(`Can't find ${req.originalUrl} on this server!`, 404))
})

app.use(errorRouter)

module.exports = app
