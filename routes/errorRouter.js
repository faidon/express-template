const express = require('express')

const router = express.Router()

router.use((err, req, res, next) => {
  if (err.statusCode === 404) {
    res.send('Page not found')
  } else {
    next()
  }
})

module.exports = router
