# Express Templaste

Install dependencies

``` npm
npm install
```

## Vanilla JS & Sass frontend with Parcel

# Usage

Run dev server - http://localhost:3000

``` 
npm run dev
```

Build assets for production

``` 
npm run build
```

## structure

flows downwards

* setup.js (initialize express app) 
* security.js (setup security measures)
* testing.js (for testing middleware)
* app.js (routing middleware, app logic)
* server.js (init server)
