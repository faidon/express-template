class Operational extends Error {
  constructor(message, statusCode, extra) {
    super(message)

    this.statusCode = statusCode
    this.isOperational = true
    this.extra = extra

    Error.captureStackTrace(this, this.constructor)
  }
}

module.exports = Operational
